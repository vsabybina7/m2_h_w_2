// Задание
// Написать реализацию функции, которая позволит создавать объекты типа Hamburger, используя возможности стандарта ES5.
//
//     Технические требования:
//     Некая сеть фастфудов предлагает два вида гамбургеров:

// маленький (50 гривен, 20 калорий)
// большой (100 гривен, 40 калорий)
//
// Гамбургер должен включать одну дополнительную начинку (обязательно):
//
// сыр (+ 10 гривен, + 20 калорий)
// салат (+ 20 гривен, + 5 калорий)
// картофель (+ 15 гривен, + 10 калорий)
//
// Дополнительно, в гамбургер можно добавить приправу (+ 15 гривен, 0 калорий) и полить майонезом (+ 20 гривен, + 5 калорий).
//
//
// Необходимо написать программу, рассчитывающую стоимость и калорийность гамбургера. Обязательно нужно использовать ООП подход (подсказка: нужен класс Гамбургер, константы, методы для выбора опций и рассчета нужных величин).
//
//
// Код необходимо написать под стандарт ES5.
//
//     Код должен быть защищен от ошибок. Представьте, что вашим классом будет пользоваться другой программист. Если он передает неправильный тип гамбургера, например, или неправильный вид добавки, должно выбрасываться исключение (ошибка не должна молча игнорироваться).
//
//
// Написанный класс должен соответствовать следующему jsDoc описанию (то есть содержать указанные методы, которые принимают и возвращают данные указанного типа и выбрасывают исключения указанного типа. Комментарии ниже тоже можно скопировать в свой код):

//*************** Home Work 1 ******************//

function Hamburger(size, stuffing, topping) {
    this.size = size;
    this.stuffing = stuffing;
    this.topping = []
}
//*** size ***//
    Hamburger.SIZE_SMALL = {
        price: 50,
        callories: 20,
    }
    Hamburger.SIZE_LARGE = {
        price: 100,
        callories: 40
    }
//***stuffing***//
    Hamburger.STUFFING_CHEESE = {
        price: 10,
        callories: 20,
    }
    Hamburger.STUFFING_SALAD = {
        price: 20,
        callories: 5,
    }
    Hamburger.STUFFING_POTATO = {
        price: 15,
        callories: 10,
    }
//***topping***//
    Hamburger.TOPPING_MAYO = {
        price: 15,
        callories: 0,
    }
    Hamburger.TOPPING_SPICE = {
        price: 20,
        callories: 5,
    }


    Hamburger.prototype.addTopping = function (topping) {
        return this.topping.push(topping)
    };

    Hamburger.prototype.removeTopping = function (topping) {
        return this.topping.splice(topping, 1)
    }

    Hamburger.prototype.getToppings = function () {
        return  this.topping
    }

    Hamburger.prototype.getSize = function (){
        return  this.size
    }


    Hamburger.prototype.getStuffing = function () {
        return this.stuffing
    }


    Hamburger.prototype.calculateCalories = function () {
        let result = this.size.callories + this.stuffing.callories;
        for(let topping of this.topping){
            result += topping.callories
        }
        return result;
    }

    Hamburger.prototype.calculatePrice = function () {
        let result = this.size.price + this.stuffing.price;
       for(let topping of this.topping){
           result += topping.price
       }
        return result;
    }


/**
 * Представляет информацию об ошибке в ходе работы с гамбургером.
 * Подробности хранятся в свойстве message.
 * @constructor
 */
function HamburgerExeption(msg){
    this.name = "Hamburger Exeption";
    this.message = msg;
}


let burger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE)
console.log(burger);


console.log("Price: %f", burger.calculatePrice())
console.log("Calories: %f", burger.calculateCalories());

burger.addTopping(Hamburger.TOPPING_SPICE);
console.log("Price with sauce: %f", burger.calculatePrice());

console.log("Is hamburger large: %s", burger.getSize() === Hamburger.SIZE_LARGE);

burger.removeTopping(Hamburger.TOPPING_SPICE);
console.log("Have %d toppings", burger.getToppings().length);




